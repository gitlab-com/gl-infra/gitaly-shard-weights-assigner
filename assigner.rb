# frozen_string_literal: true

require 'set'
require 'json'
require 'net/http'

BALANCING_PROMETHEUS_QUERY = 'sort_desc(instance:node_filesystem_avail:ratio{env="<env>",type="gitaly",mountpoint="/var/opt/gitlab",shard=~"default",monitor="default", fqdn=~"gitaly-.*"} > <cutoff>)'
GITALY_PODS_QUERY = 'sort_desc((kubelet_volume_stats_available_bytes{env="<env>",namespace="gitlab",persistentvolumeclaim=~".*gitaly.*"} / kubelet_volume_stats_capacity_bytes{env="<env>",namespace="gitlab",persistentvolumeclaim=~".*gitaly.*"}) > <cutoff>)'
SHARD_CUTOFF_PERCENTAGE = 0.20 # Select shards whose free disk percentage above this value
TARGET_INSTANCE = ENV['TARGET_INSTANCE']

@application_settings = nil

def log(msg)
  puts "[ASSIGNER] #{msg}"
end

def check_required_envvars
  %w[
    MIMIR_URL
    MIMIR_API_USER
    MIMIR_API_PASSWORD
    TARGET_INSTANCE
    TARGET_API_PRIVATE_TOKEN
  ].each do |envvar|
    raise "Missing envvar #{envvar}" unless ENV.key?(envvar)
  end
end

def call_gitlab_api(method, resource, payload = nil)
  uri      = URI("https://#{TARGET_INSTANCE}/api/v4/#{resource}")
  response = request = nil
  headers  = {
    'Private-Token' => ENV['TARGET_API_PRIVATE_TOKEN'],
    'Content-Type' => 'application/json'
  }

  log("Calling GitLab API (method = #{method}, path = #{uri.path}, payload = #{payload.inspect})")

  Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
    case method
    when :get
      request = Net::HTTP::Get.new(uri.path, headers)
    when :put
      request = Net::HTTP::Put.new(uri.path, headers)
      request.body = JSON.generate(payload)
    else
      raise "Unsupported method #{method}"
    end

    response = http.request(request)
  end

  unless response.is_a?(Net::HTTPSuccess)
    raise "Failed querying GitLab API for #{resource}, body is #{response.body.inspect}"
  end

  JSON.parse(response.body)
end

def environment_prometheus_identifier
  case TARGET_INSTANCE
  when 'gitlab.com' then 'gprd'
  when 'staging.gitlab.com' then 'gstg'
  else raise("Couldn't identify a Prometheus environment for #{TARGET_INSTANCE}")
  end
end

def mimir_tenant_id
  case TARGET_INSTANCE
  when 'gitlab.com' then 'gitlab-gprd'
  when 'staging.gitlab.com' then 'gitlab-gstg'
  else raise("Couldn't identify a Mimir tenant ID for #{TARGET_INSTANCE}")
  end
end

def query_mimir(query)
  uri = URI("#{ENV['MIMIR_URL']}/api/v1/query")
  params = {
    query: query,
    time: Time.now.to_f
  }
  uri.query = URI.encode_www_form(params)

  headers = {
    'X-Scope-OrgID' => mimir_tenant_id,
  }

  log("Querying Mimir at #{ENV['MIMIR_URL']} for #{query}")

  response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
    request = Net::HTTP::Get.new(uri, headers)
    request.basic_auth(ENV['MIMIR_API_USER'], ENV['MIMIR_API_PASSWORD'])
    http.request(request)
  end

  raise "Failed querying Mimir. Response: #{response.body}" unless response.is_a?(Net::HTTPSuccess)

  mimir_response = JSON.parse(response.body)
  raise 'Mimir returned a status that is not success' unless mimir_response['status'] == 'success'

  mimir_response
end

def shards_ordered_by_available_disk_space
  query = BALANCING_PROMETHEUS_QUERY
          .gsub('<env>', environment_prometheus_identifier)
          .gsub('<cutoff>', SHARD_CUTOFF_PERCENTAGE.to_s)

  mimir_response = query_mimir(query)

  mimir_response['data']['result'].map do |item|
    [
      # FQNDs that start with gitaly use the full FQDN as the
      # storage name
      if item['metric']['fqdn'].start_with?('gitaly')
        item['metric']['fqdn']
      else
        item['metric']['fqdn'].gsub(/-stor-g.*$/,
                                    '')
      end,
      item['value'][1].to_f
    ]
  end
end

def gitaly_pods_ordered_by_available_volume_space
  query = GITALY_PODS_QUERY
          .gsub('<env>', environment_prometheus_identifier)
          .gsub('<cutoff>', SHARD_CUTOFF_PERCENTAGE.to_s)

  mimir_response = query_mimir(query)

  mimir_response['data']['result'].map do |item|
    # A volume name looks like this: repo-data-gitaly-k8s-00-stor-gstg-gitlab-36dv2-gitaly-0
    derived_name = item['metric']['persistentvolumeclaim'].match(/^repo-data-(.*)-gitaly-\d$/)[1]
    shard_name = @application_settings['repository_storages_weighted'].keys.find { |shard_name| shard_name.include?(derived_name) }

    [shard_name, item['value'][1].to_f]
  end
end

def excluded_shards_set
  Set.new(ENV['EXCLUDED_SHARDS'].to_s.split(','))
end

def shard_weights_per_mimir
  maximum_balanced_shards = (ENV['MAXIMUM_BALANCED_SHARDS'] || 5).to_i

  all_shards = shards_ordered_by_available_disk_space
    .concat(gitaly_pods_ordered_by_available_volume_space)
    .sort_by { |(_, percentage)| percentage }
    .reverse
  log("Available shards are #{all_shards.map(&:first).join(', ')}")

  excluded_shards = excluded_shards_set

  log("Excluding shards #{excluded_shards.to_a.join(', ')} from Mimir response ...")
  available_shards = all_shards
                     .reject { |name, _percentage| excluded_shards.include?(name) }
                     .first(maximum_balanced_shards)

  available_space_max_percentage = available_shards[0][1] # The list sorted by shard percentage descendingly.
  available_shards.map! do |(name, percentage)|
    [
      name,
      ((percentage / available_space_max_percentage) * 100).ceil
    ]
  end
  available_shards.to_h
end

def shard_weights_per_the_application
  @application_settings ||= call_gitlab_api(:get, 'application/settings')

  @application_settings['repository_storages_weighted']
end

def assign_new_weights(weights)
  if ENV['DRY_RUN']
    log('Skipping assigning new weights because DRY_RUN envvar is set')
    return
  end

  if weights.all? { |(_, weight)| weight == 0 }
    log('Welp! All weights ended up being zero, stopping before I mess things up!')
    exit 1
  end

  call_gitlab_api(:put, 'application/settings', { repository_storages_weighted: weights })
end

def notify_slack(old_weights, new_weights)
  return unless ENV['SLACK_WEBHOOK_URL']

  preamble     = 'The following Gitaly shards have been assigned these weights:'
  footer       = ":ci_passing: This message was sent from this <#{ENV['CI_JOB_URL']}|CI job>"
  weights_list = new_weights.map do |(shard, weight)|
    next if old_weights[shard] == weight

    "• `#{shard}`: #{weight}% (was #{old_weights[shard]}%)"
  end.compact.join("\n")

  if ENV['DRY_RUN']
    puts "This message would've been sent to Slack"
    puts "URL: #{ENV['SLACK_WEBHOOK_URL']}"
    puts "Channel: #{ENV['SLACK_CHANNEL']}"
    puts '=' * 80
    puts [preamble, weights_list, footer].join("\n")
    return
  end

  uri     = URI(ENV['SLACK_WEBHOOK_URL'])
  request = Net::HTTP::Post.new(uri)

  payload = {
    blocks: [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: preamble
        }
      },
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: weights_list
        }
      },
      {
        type: 'context',
        elements: [
          {
            type: 'mrkdwn',
            text: footer
          }
        ]
      }
    ],
    channel: ENV['SLACK_CHANNEL']
  }
  request.set_form_data('payload' => JSON.generate(payload))

  Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
    response = http.request(request)
    response.body
  end

  log('Notification sent to Slack')
end

def output_new_weights_as_prometheus_metrics(new_weights)
  file_name = 'new-weights.prom'

  File.open(file_name, 'w') do |f|
    f.write("# HELP gitaly_shard_weight_total The weight assigned to a Gitaly shard from Rails POV\n")
    f.write("# TYPE gitaly_shard_weight_total gauge\n")

    metrics = new_weights.map do |(shard, weight)|
      "gitaly_shard_weight_total{shard_name=\"#{shard}\",env=\"#{environment_prometheus_identifier}\"} #{weight}"
    end.join("\n")

    f.write(metrics)
    f.write("\n")
  end

  log "Prometheus metrics for new weights are in #{file_name}"
end

def get_old_and_new_weights
  old_weights = shard_weights_per_the_application
  mimir_weights = shard_weights_per_mimir
  excluded_shards = excluded_shards_set

  new_weights = old_weights.map do |(shard, old_weight)|
    if (new_weight = mimir_weights[shard])
      [shard, new_weight]
    elsif excluded_shards.include?(shard)
      [shard, old_weight]
    else
      # Shards that are neither in the Mimir result nor excluded should be
      # zeroed to avoid "leaking" shards (e.g. shards that no longer make the cut-off).
      [shard, 0]
    end
  end.to_h

  [old_weights, new_weights]
end

def main
  check_required_envvars

  old_weights, new_weights = get_old_and_new_weights
  log("Old weights = #{old_weights}")
  log("New weights = #{new_weights}")

  output_new_weights_as_prometheus_metrics(new_weights)

  if new_weights == old_weights
    log('No changes in the weights, exiting ...')
    exit 0
  end

  assign_new_weights(new_weights)
  notify_slack(old_weights, new_weights)
end

main
